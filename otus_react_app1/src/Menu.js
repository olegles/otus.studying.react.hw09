import React from 'react';

function Menu({sitename}){
    return (
        <div>
            <a href="/">Home</a> |
            <a href="/about">About</a> &nbsp;
            {sitename}
        </div>
    )
}

export default Menu