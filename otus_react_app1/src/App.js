import logo from './logo.svg';
import './App.css';
import Menu from './Menu';
import Footer from './Footer';
import LoginForm from './LoginForm';

function App() {

  const size = 2;
  const dateNow = Date.now();

  //axios.get('http://localhost:5005/api')
  //fetch('http://localhost:5005/api/')

  return (
    <div className="App">
      <Menu sitename='Page 1  '/>
      <Menu sitename={dateNow}/>

      {[1,2,3,4].map(i =>(
         <Menu sitename={i}/>
      ))}

      <Menu sitename='Contact us  '/>      
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React every  {size} days per week.
        </a>
      </header>
      <br/>
      <LoginForm />
      <br/>
      <Footer/>
      <Footer showMenu/>
      <Footer showMenu additionalText={'Hello world'}/>
    </div>
  );
}

export default App;
