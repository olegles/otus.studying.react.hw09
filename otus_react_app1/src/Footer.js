import propTypes from 'prop-types';

function Footer({showMenu, additionalText}){
    return (
        <footer>
            {showMenu ? <div>
                <a href="/">Home</a> |
                <a href="/">about</a>
            </div> : null}
            (C) 2023
            &nbsp;&nbsp;&nbsp; {additionalText}
        </footer>
    )
}

Footer.defaultProps = {
    showMenu: true,
    additionalText:'<MISSING TEXT>'
}

Footer.propTypes = {
    showMenu: propTypes.bool,
    additionalText: propTypes.string
}

export default Footer