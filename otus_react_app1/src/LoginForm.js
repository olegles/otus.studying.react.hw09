import React, { useState } from "react";
import axios from "axios";

function LoginForm() {
  const [LogIned, setLogIned] = useState(false);
  const [userName, setUserName] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    axios.post(`post`).then((result) => {
      if (result.status === 200) {
        setLogIned(true);
        setUserName(e.target.username.value);
      }
    });

  };

  return (
    <>

      {LogIned && <div>{userName}, Приветствуем!</div>}
      {!LogIned && (
        <container align = "center">
          <span><b>Login form</b></span>          
          <form onSubmit={onSubmit}>
            <label>
              Login: <input type="text" name="username" />
            </label>
            <br />
            <label>
              Password: <input type="password" name="userpassword" />
            </label>
            <br />
            <input type="submit" value="Enter" />
          </form>
        </container>
      )}
    </>
  );
}
export default LoginForm;
